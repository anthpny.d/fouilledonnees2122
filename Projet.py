#encoding=utf8

import string
from random import random
from datetime import datetime
from datetime import timedelta
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

"""
Anthony
Dupré
"""
global coeffs

def init(listeCoeff):
	global coeffs
	coeffs=listeCoeff

def coeffsValide():
	global coeffs
	cmptZero=0
	for i in range(len(coeffs)):
		if coeffs[i]==0:
			cmptZero+=1
	if cmptZero==len(coeffs):
		return False
	changement=False
	indice=len(coeffs)-1
	while not changement:
		if coeffs[indice]!=0:
			coeffs[indice]=coeffs[indice]-0.5
			i=indice+1
			while i<len(coeffs):
				coeffs[i]=1
				i+=1
			changement=True
		indice-=1
	return True

init([1,1,1,1])

global dicoAnime
dicoAnime=dict()
global dicoGenre
dicoGenre=dict()
global dicoStudio
dicoStudio=dict()
global cosineName
global listePairDate
listePairDate=[]

"""
Ags:rien
Sortie:rien
Lis le fichier animeComplete.csv et remplir les variables globales
"""
def readAnime():
	listeName=[]
	for line in open("animeReduce.csv", 'r').readlines():
		line=cleanLine(line).split(',')
		dicoAnime[int(line[0])]=line[1:]
		fillDicoGenre(int(line[0]),line[2])
		fillDicoStudio(int(line[0]),line[3])
		listeName.append(line[1])
	fillHistogrammeDate()
	calculCosineName(listeName)

"""
Ags:id->int,listeGenre->list_str
Sortie:rien
Insere dans le dicoGenre l'anime avec l'identifiant idAnime dans les valeurs des clés présentes dans listeGenre
"""
def fillDicoGenre(idAnime,listeGenre):
	listeGenre=listeGenre.replace('"','').replace(' ','').split(';')
	for genre in listeGenre:
		if(not(genre in dicoGenre)):
			dicoGenre[genre]=[idAnime]
		else:
			dicoGenre[genre]=dicoGenre[genre]+[idAnime]

"""
Ags:id->int,listeStudio->list_str
Sortie:rien
Insere dans le dicoStudio l'anime avec l'identifiant idAnime dans les valeurs des clés présentes dans listeStudio
"""
def fillDicoStudio(idAnime,listeStudio):
	listeStudio=listeStudio.replace('"','').replace(' ','').split(';')
	for studio in listeStudio:
		if(not(studio in dicoStudio)):
			dicoStudio[studio]=[idAnime]
		else:
			dicoStudio[studio]=dicoStudio[studio]+[idAnime]

"""
Ags:listeType->list_str
Sortie:dico->dict
Insere dans le dicoGenre l'anime avec l'identifiant idAnime dans les valeurs des clés présentes dans listeGenre
"""
def getAnimeByCommonType(liste):
	dico=dict()
	for i in range(len(liste)):
		dico[i]=dicoGenre[liste[i].replace(" ","")]
	return dico

"""def getAnimeByCommonType2(listeType):
	dico=dict()
	for i in range(len(listeType)+1):
		dico[i]=[]
	for anime in list(dicoAnime.keys()):
		nbTypeCommun=0
		for genre in listeType:
			if anime in dicoGenre[genre]:
				nbTypeCommun+=1
		dico[nbTypeCommun]=dico[nbTypeCommun]+[anime]
	return dico"""

"""
Ags:listeName->list_str
Sortie:rien
Calcule le cosine_similarity par rapport aux noms des animes
"""
def calculCosineName(listeName):
	global cosineName
	vectorizer = TfidfVectorizer()
	cosineName=cosine_similarity(vectorizer.fit_transform(listeName))


"""
Ags:listIdAnime->list_int
Sortie:rien
Calcule le cosine_similarity par rapport aux noms des animes
"""
def findSimilaryName(listIdAnime):
	global cosineName
	listeCles=list(dicoAnime.keys())
	listeCles.sort()
	listeSimilary=[0]*len(listeCles)
	for idAnime in listIdAnime:
		liste=cosineName[listeCles.index(idAnime)]
		for i in range(len(liste)):
			listeSimilary[i]+=liste[i]
	for j in range(len(listeSimilary)):
		listeSimilary[j]/=len(listeCles)
	return listeSimilary

def findSimilaryStudio(liste):
	dico=dict()
	for i in range(len(liste)):
		dico[i]=dicoStudio[liste[i].replace(" ","")]
	return dico

def findSimilaryAnime(listIdAnime):
	listGenre=[]
	listStudio=[]
	listeAnimeCoeff=[]
	listeName=findSimilaryName(listIdAnime)
	moyYear=0
	nbYear=0
	for d,idAnime in listePairDate:
		if idAnime in listIdAnime:
			moyYear+=d.year
			nbYear+=1
	if(nbYear!=0):
		moyYear//=nbYear
	d=str(moyYear)+"/01/07"
	erreurDate=False
	try:
		dateFind=datetime.strptime(d, '%Y/%m/%d').date()
		dicoYear=findNearDate(dateFind)
	except ValueError:
		erreurDate=True
	for i in listIdAnime:
		for genre in dicoAnime[i][1].replace("\"","").split(';'):
			listGenre.append(genre)
		for studio in dicoAnime[i][2].replace("\"","").split(';'):
			listStudio.append(studio)
	listGenre=transformListStringToPair(listGenre)
	listStudio=transformListStringToPair(listStudio)
	dicoSimStudio=findSimilaryStudio(listStudio)
	dicoSimType=getAnimeByCommonType(listGenre)
	for idAnime in list(dicoAnime.keys()):
		if(erreurDate):
			coeffYear=0
		else:
			coeffYear=getCoeffYear(dicoYear,idAnime)
		coeffGenre=getCoeffGenre(dicoSimType,idAnime)
		coeffStudio=getCoeffStudio(dicoSimStudio,idAnime)
		coeffName=listeName[list(dicoAnime.keys()).index(idAnime)]
		listeAnimeCoeff.append((coeffs[0]*coeffYear+coeffs[1]*coeffGenre+coeffs[2]*coeffStudio+coeffs[3]*coeffName,idAnime))
	return listeAnimeCoeff

def getCoeffYear(dicoYear,idAnime):
	i=0
	while not(idAnime in dicoYear[list(dicoYear.keys())[i]]) and i<len(list(dicoYear.keys())):
		i+=1
	return 1/(1+abs(list(dicoYear.keys())[i]))

def getCoeffGenre(dico,idAnime):
	i=0
	num=1
	deno=1
	while i<len(list(dico.keys())):
		if idAnime in dico[list(dico.keys())[i]]:
			num+=1/(i+1)
		deno+=1/(i+1)
		i+=1	
	if num==1:
		num=0
	return num/deno

def getCoeffStudio(dico,idAnime):
	i=0
	num=1
	deno=1
	while i<len(list(dico.keys())):
		if idAnime in dico[list(dico.keys())[i]]:
			num+=1/(i+1)
		deno+=1/(i+1)
		i+=1
	if num==1:
		num=0		
	return num/deno

def findNearDate(dateFind):
	listeIntervalles=[1,3,5,8,12,15,20]
	dicoIntervalle=dict()
	dicoIntervalle[0]=list(dicoAnime.keys())
	for i in listeIntervalles:
		dicoIntervalle[i]=list()
		dicoIntervalle[-i]=list()
	for d,idAnime in listePairDate:	
		if d<dateFind:
			indice=len(listeIntervalles)-1
			delta=timedelta(days=listeIntervalles[indice]*365)
			while indice>=0 and d+delta>dateFind:
				indice-=1
				delta=timedelta(days=listeIntervalles[indice]*365)
			if(indice!=-1):
				dicoIntervalle[-listeIntervalles[indice]].append(idAnime)
				if idAnime in dicoIntervalle[0]:
					dicoIntervalle[0].remove(idAnime)
		elif d>dateFind:
			indice=len(listeIntervalles)-1
			delta=timedelta(days=listeIntervalles[indice]*365)
			while indice>=0 and dateFind+delta>d:
				indice-=1
				delta=timedelta(days=listeIntervalles[indice]*365)
			if(indice!=-1):
				dicoIntervalle[listeIntervalles[indice]].append(idAnime)
				if idAnime in dicoIntervalle[0]:
					dicoIntervalle[0].remove(idAnime)
	return dicoIntervalle

def transformListStringToPair(liste):
	newList=[]
	while(len(liste)!=0):
		element=liste[0]
		nbElement=0
		while element in liste:
			nbElement+=1
			liste.remove(element)
		newList.append((nbElement,element))
	newList.sort()
	newList.reverse()
	for i in range(len(newList)):
		_,x=newList[i]
		newList[i]=x
	return newList	

"""
Ags:listeObtenue->list_int,listeAttendu->list_int
Sortie:(recall,precision)->(float,float)
Calcule le recall et la precision d'une liste par rapport à la liste attendu
"""
def getRecallPrecision(listeObtenue,listeAttendu):
	if len(listeAttendu)==0:
		return (0,0)
	nbTrouve=0
	for element in listeObtenue:
		if element in listeAttendu:
			nbTrouve+=1
	recall=nbTrouve/len(listeAttendu)
	precision=nbTrouve/len(listeObtenue)
	return (recall,precision)
	
def cleanLine(line):
	betweenCote=False
	for i in range(len(line)):
		if(line[i]=='"'):
			betweenCote=not(betweenCote)
		elif betweenCote and line[i]==',':
			liste=list(line)
			liste[i]=';'
			line="".join(liste)
	return line

def fillHistogrammeDate():
	today=datetime.strptime('2021/11/27', '%Y/%m/%d').date()
	for cle in list(dicoAnime.keys()):
		try:
			date_object = datetime.strptime(dicoAnime[cle][7], '%Y/%m/%d').date()
		except ValueError:
			try :
				date_object = datetime.strptime(dicoAnime[cle][7], '%b-%y').date()
				if(today<date_object):
					tab=dicoAnime[cle][7].split('-')
					print(tab)
					newDate="-19".join(tab)
					date_object = datetime.strptime(dicoAnime[cle][7], '%b-%Y').date()
			except ValueError:
				try:
					date_object = datetime.strptime(dicoAnime[cle][7], '%y-%b').date()
					if(today<date_object):
						newDate="19"+dicoAnime[cle][7]
						print(newDate)
						date_object = datetime.strptime(dicoAnime[cle][7], '%Y-%b').date()
				except ValueError:
					try:
						str_date="0"+dicoAnime[cle][7]
						date_object = datetime.strptime(str_date, '%y-%b').date()
					except ValueError:
						print("erreur",cle)
		listePairDate.append((date_object,cle))
	listePairDate.sort()
	
def split_file():
	fileoutTrain=open("ratings_train.csv",'w+')
	fileoutTest=open("ratings_test.csv",'w+')
	currentUser=None
	for line in open("ratingsReduce.csv", 'r').readlines()[1:]:
		lineSplit=line.split(',')
		if(int(lineSplit[0])!=currentUser):
			currentUser=int(lineSplit[0])
			alea=random()
		if(alea<0.66):
			fileoutTrain.write(line)
		else:
			fileoutTest.write(line)	

def getCoeffPasReco(liste,idAnime):
	indice=0
	coeff,idA=liste[indice]
	while(idA!=idAnime):
		coeff,idA=liste[indice]
		indice+=1
	print(coeff," coeff")
	return coeff

def read_train_files():
	global coeffs
	currentUser=None
	listeTrain=list()
	listeTrainEvitee=list()
	listeTest=list()
	listeRecallPrecision=list()
	for line in open("ratings_train.csv", 'r').readlines():
		lineSplit=line.split(',')

		if currentUser is None:
			currentUser=int(lineSplit[0])

		elif(int(lineSplit[0])!=currentUser):
			listeReco=findSimilaryAnime(listeTrain)
			listeEvitee=findSimilaryAnime(listeTrainEvitee)
			listeDefReco=list()
			for coeff,idAnimeReco in listeReco:
				indiceReco=0
				coeffEvitee,idAnimeEvitee=listeEvitee[indiceReco]
				while(idAnimeReco!=idAnimeEvitee):
					indiceReco+=1
					coeffEvitee,idAnimeEvitee=listeEvitee[indiceReco]
				listeDefReco.append((coeff-coeffEvitee,idAnimeEvitee))
			listeDefReco.sort()
			listeDefReco.reverse()
			indice=0
			listeFind=list()
			while(indice<len(listeDefReco)):
				coeff,idAnime=listeDefReco[indice]
				if not idAnime in listeTrain:
					listeFind.append(idAnime)
				indice+=1
			copyCoeffs=coeffs.copy()
			listeRecallPrecision.append((copyCoeffs,getRecallPrecision(listeFind[:100],listeTest)))

			if(not coeffsValide()):
				return listeRecallPrecision

			listeTrain=list()
			listeTrainEvitee=list()
			listeTest=list()
			currentUser=int(lineSplit[0])

		if random()<0.3:
			if(int(lineSplit[2])>5):
				listeTrain.append(int(lineSplit[1]))
			elif(int(lineSplit[2])<5):
				listeTrainEvitee.append(int(lineSplit[1]))
		else:
			listeTest.append(int(lineSplit[1]))
	return None

def read_test_files(coefficients):
	currentUser=None
	nbUser=0
	listeTrain=list()
	listeTrainEvitee=list()
	listeTest=list()
	recall=0
	precision=0
	init(coefficients)
	for line in open("ratings_test.csv", 'r').readlines():
		lineSplit=line.split(',')

		if currentUser is None:
			currentUser=int(lineSplit[0])
			nbUser+=1

		elif(int(lineSplit[0])!=currentUser):
			listeReco=findSimilaryAnime(listeTrain)
			listeEvitee=findSimilaryAnime(listeTrainEvitee)
			listeDefReco=list()
			for coeff,idAnimeReco in listeReco:
				indiceReco=0
				coeffEvitee,idAnimeEvitee=listeEvitee[indiceReco]
				while(idAnimeReco!=idAnimeEvitee):
					indiceReco+=1
					coeffEvitee,idAnimeEvitee=listeEvitee[indiceReco]
				listeDefReco.append((coeff-coeffEvitee,idAnimeEvitee))
			listeDefReco.sort()
			listeDefReco.reverse()
			indice=0
			listeFind=list()
			while(indice<len(listeDefReco)):
				coeff,idAnime=listeDefReco[indice]
				if not idAnime in listeTrain:
					listeFind.append(idAnime)
				indice+=1
			reca,prec=getRecallPrecision(listeFind[:100],listeTest)
			recall+=reca
			precision+=prec

			if(nbUser!=10):
				return (recall/nbUser,precision/nbUser)

			listeTrain=list()
			listeTrainEvitee=list()
			listeTest=list()
			currentUser=int(lineSplit[0])
			nbUser+=1

		if random()<0.3:
			if(int(lineSplit[2])>5):
				listeTrain.append(int(lineSplit[1]))
			elif(int(lineSplit[2])<5):
				listeTrainEvitee.append(int(lineSplit[1]))
		else:
			listeTest.append(int(lineSplit[1]))
	return None

"""
def read_train_files():
	currentUser=None
	listeTrain=list()
	listeTest=list()
	listeRecallPrecision=list()
	listNbReco=[20,50,100,200,500,1000]
	indiceNbReco=0
	for line in open("ratings_train.csv", 'r').readlines():
		lineSplit=line.split(',')

		if currentUser is None:
			currentUser=int(lineSplit[0])

		elif(int(lineSplit[0])!=currentUser):
			listeReco=findSimilaryAnime(listeTrain)
			listeReco.sort()
			listeReco.reverse()

			indice=0
			listeFind=list()
			while(indice<len(listeReco)):
				coeff,idAnime=listeReco[indice]
				if not idAnime in listeTrain:
					listeFind.append(idAnime)
				indice+=1
			listeRecallPrecision.append((listNbReco[indiceNbReco],getRecallPrecision(listeFind[:listNbReco[indiceNbReco]],listeTest)))
			indiceNbReco+=1
			listeTrain=list()
			listeTest=list()
			if random()<0.3:
				if(int(lineSplit[2])>5 or int(lineSplit[1])==-1):
					listeTrain.append(int(lineSplit[1]))
			else:
				listeTest.append(int(lineSplit[1]))
			currentUser=int(lineSplit[0])
			if(indiceNbReco==len(listNbReco)):
				return listeRecallPrecision
		else:
			if random()<0.3:
				if(int(lineSplit[2])>5 or int(lineSplit[1])==-1):
					listeTrain.append(int(lineSplit[1]))
			else:
				listeTest.append(int(lineSplit[1]))
	return None
"""