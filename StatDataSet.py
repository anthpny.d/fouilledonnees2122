def getStat():
	print("Nombres de lignes dans anime.csv = ",getNbLinesAnime())

def getNbLinesAnime():
	return len(open("anime2.csv", 'r').readlines()[1:])

def statRanking(file):
	histogrammeUser=[0]*6
	nbUser=0
	currentUser=-1
	nbAnime=0
	listeLine=open(file, 'r').readlines()[1:]
	for line in listeLine:
		if currentUser==-1:
			currentUser=int(line.split(',')[0])
			nbUser+=1
			nbAnime=1
		elif currentUser!=int(line.split(',')[0]):
			caseHistogramme=nbAnime//50
			if caseHistogramme>5:
				caseHistogramme=5
			histogrammeUser[caseHistogramme]+=1
			currentUser=int(line.split(',')[0])
			nbUser+=1
			nbAnime=1
		else:
			nbAnime+=1
	print("Il y a ",len(listeLine)," lignes dans ",file)
	print("Il y a ",nbUser," dans le dataset")
	print(histogrammeUser[0], " ont regarde entre 0 et 50 animes")
	print(histogrammeUser[1], " ont regarde entre 50 et 100 animes")
	print(histogrammeUser[2], " ont regarde entre 100 et 150 animes")
	print(histogrammeUser[3], " ont regarde entre 150 et 200 animes")
	print(histogrammeUser[4], " ont regarde entre 200 et 250 animes")
	print(histogrammeUser[5], " ont regarde plus de 250 animes")