import string
from string import digits

def createAnimeCSV():
	anime1=open("anime.csv", 'r').readlines()
	anime2=open("anime2.csv", 'r').readlines()
	fileout=open("animeComplete.csv",'w+')
	cursor1=1
	cursor2=1
	while(cursor1<len(anime1) and cursor2<len(anime2)):
		line1=cleanLine(anime1[cursor1])
		line2=cleanLine(anime2[cursor2])
		listAnime1=line1.split(',')
		listAnime2=line2.split(',')
		if(listAnime1[0]==listAnime2[0]):
			line=listAnime2[:3]+[listAnime1[12]]+listAnime2[3:]
			fileout.write(','.join(line))
			cursor1+=1
			cursor2+=1
		else:
			if int(listAnime1[0])<int(listAnime2[0]):
				cursor1+=1
			else:
				cursor2+=1

def createRatingsCSV():
	anime=open("animeComplete.csv", 'r').readlines()
	listId=[]
	for line in anime:
		line=line.split(',')
		listId.append(int(line[0]))
	ratings=open("ratings.csv",'r').readlines()[1:]
	fileout=open("ratingsComplete.csv",'w+')
	print("entree")
	for line in ratings:
		print(line)
		line=line.split(',')
		if int(line[1]) in listId:
			print("add")
			fileout.write(','.join(line))

def createAnimeSynopsisCSV():
	wordClean=[" a "," an "," but "," of "," in "," the "," is "," are "," by "," as "," to "," it "," and "]
	anime1=open("animeComplete.csv", 'r').readlines()
	anime2=open("other_file/anime_with_synopsis_clean.csv", 'r').readlines()
	fileout=open("animeCompleteSynopsis.csv",'w+')
	cursor1=0
	cursor2=0
	while cursor1<len(anime1):
		line1=cleanLine(anime1[cursor1])
		line2=cleanLine(anime2[cursor2])
		listAnime1=line1.replace("\n","").split(',')
		listAnime2=line2.split(',')
		if(listAnime1[0]==listAnime2[0]):
			syn=listAnime2[4]
			syn=syn.lower()
			syn=syn.translate(str.maketrans('', '', string.punctuation))
			syn= syn.translate(str.maketrans('', '', digits))
			for word in wordClean:
				syn=syn.replace(word," ")
			tab=listAnime1+[syn]
			fileout.write(','.join(tab))
			cursor1+=1
			cursor2+=1
		else:
			if int(listAnime1[0])<int(listAnime2[0]):
				tab=listAnime1+["\n"]
				fileout.write(','.join(tab))
				cursor1+=1
			else:
				cursor2+=1

def reduceAnimes():
	listAnime=list()
	listToWrite=list()
	fileout=open("animeReduce.csv",'w+')
	for line in open("animeCompleteSynopsis.csv", 'r').readlines():
		tabLine=line.split(',')
		listAnime.append((float(tabLine[7]),tabLine))
		print(tabLine[7])
	listAnime.sort()
	listAnime.reverse()
	listAnime=listAnime[:4000]
	for _,anime in listAnime:
		listToWrite.append((int(anime[0]),anime))
	listToWrite.sort()
	for _,toWrite in listToWrite:
		print(toWrite)
		fileout.write(','.join(toWrite))

def createRatingsReduce():
	anime=open("animeReduce.csv", 'r').readlines()
	listId=[]
	for line in anime:
		line=line.split(',')
		listId.append(int(line[0]))
	ratings=open("ratingsComplete.csv",'r').readlines()[1:]
	fileout=open("ratingsReduce.csv",'w+')
	for line in ratings:
		line=line.split(',')
		if int(line[1]) in listId:
			fileout.write(','.join(line))

def cleanLine(line):
	betweenCote=False
	for i in range(len(line)):
		if(line[i]=='"'):
			betweenCote=not(betweenCote)
		elif betweenCote and line[i]==',':
			liste=list(line)
			liste[i]=';'
			line="".join(liste)
	return line

def cleanSynopsis(synopsis):
	synopsis=synopsis.lower()